---
title: Grenzwerte von Sauerstoff
---

# Grenzwerte von Sauerstoff [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 3-Medizin

| O2-Partialdruck      | Einsatz / Auswirkung                          |
| ----------- | ------------------------------------ |
| 0,16 bar`       | :Bewusstlosigkeit durch Sauerstoffmangel |
| 0,21 bar`       | Normale  Umgebungsbedingungen |
| 0,5 bar    | Bei langer Einwirkdauer kommt es zu Lungenschädigungen|
| 1,4 bar    | Max O2-Belastung bei normalen Tauchgängen|

