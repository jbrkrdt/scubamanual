---
title: Gasanalyse
---

# Gasanalyse [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

- Flascheninhalt selbst analysieren[^3] (Vermeidung von Unfällen durch falsche Gemische oder Verwechslungen)
- **Einweisung** in Messgerät und -verfahren (typ. von Basis)
- Messgerät **kalibrieren** (vor der Messung mit Luft kalibrieren)
- Atemgemisch **messen**
    - Ventil öffnen, so dass ein leichter Gasstrom fießt (keine Druckstöße)
    - Messgerät vor die Flasche halten, so das es dicht abschließt (Ausschluss von Luft)
    - Anzeigewert beobachten, bis keine Änderung mehr auftretetn
- Flasche **labeln**:
    - MOD,
    - Atemgemische, 
    - Sauerstoffgehalt [^2]
    - Datum und Unterschrift

[^3]:+- 1% Abweichung vom Sollwert zulässig)
[^2]: Es hat sich eingebürgert, den Sauerstoffgehalt mit einer Nachkommastelle anzugeben, um klarzustellen, dass es ein Analyseergebnis ist und von der Angabe des Zielgemisches (z.B. Nx 32)
