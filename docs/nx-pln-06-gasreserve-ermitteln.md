# 5. Ermittlung Gasreserve

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221835-ermittlung-gasreserve.svg](assets/2403221835-ermittlung-gasreserve.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow: 3">
- **Gasvolumen** = Flaschenvolumen * Fülldruck / 1bar

- davon als **Gasreserve**: 
    - Gas für Aufstieg zur Oberfläche + 50 bar Restdruck
    - 2 Taucher unter Stress aus einem Tauchgerät zur Oberfläche (Umkehrdruck)

- **Mindestdruck**: Gasreserve / Flaschenvolumen * 1 bar
</div>
</div>
