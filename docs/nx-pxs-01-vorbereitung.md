---
title: Vorbereitung (vor dem Tauchgang)
---

# Vorbereitung (vor dem Tauchgang) [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

- Planung ( geplante Tauchtiefe festlegen und Tauchgang planen, inkl.)
    - **Gasauswahl**
    - **MOD** bestimmen
- Atemgemisch **analysieren und labeln**
- **Tauchcomputer** einstellen: Luft/Nitrox?
- im Briefing besprechen: MOD, Nullzeit