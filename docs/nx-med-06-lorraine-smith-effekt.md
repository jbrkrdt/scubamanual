---
title: Lorraine-Smith-Effekt
---

# Lorraine-Smith-Effekt [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 3-Medizin

- Schädigung der Lungenalveolen
- Sauerstoffpartialdruck > 0,5 bar und sehr lange Einwirkdauer
- Einwirkdauer liegt außerhalb der Tauchzeiten beim Sporttauchen

