---
title: Membranverfahren
---
# Membranverfahren[^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung


![2303171910-Membanmethode.svg](assets/2303171910-Membanmethode.svg){width=500px align=right}	

- Sauerstoff- und Stickstoffmoleküle sind unterschiedlich groß.
- Luft wird durch eine Membrane (Bündel) geleitet
- Die Membrane wirkt als Filter: Produkte: sauerstoffreiche Luft (Nitrox) + Stickstoff
- Ein Kompressor verdichtet das Nitrox.

Vorteile: 

- einfacher, schnellere Füllen vieler Flaschen (ggf. Kaskade)
- keine Gaslogistik


Nachteile:

- ND-Kompressor + Membran + Nitrox-tauglicher Kompressor (teuer!)
- Mischungen nur bis 40% Sauerstoff möglich.


TODO Skizze membranverfahren
