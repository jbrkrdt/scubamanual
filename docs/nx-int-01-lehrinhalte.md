#  Was werden wir lernen?

In der Ausbildung zum Nitrox-GDL* sollen folgende Fragen beantwortet werden

- Was ist Nitrox?
- Welchen Effekt hat Nitrox auf den Körper?
- Wie fülle ich meine Tauchflaschen mit Nitrox?
- Was ist bei der Ausrüstung zu beachten?
- Was ist beim Tauchen mit Nitrox zu beachten?
- Wie plane ich einen Nitrox-Tauchgang?



