---
title: MOD
---

#  MOD (Maximum Operation Depth) [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 2-Physikalische Grundlagen

- MOD (Maximum Operation Depth) bezeichnet die **maximale Einsatz- bzw. Tauchtiefe** eines Atemgases.
- Für Sporttauchgänge wird eine maximaler Sauerstoffpartialdruck von 1,4 bar empfohlen.
- Mithilfe der Formel für den Partialdruck lässt sich daraus für jedes Gasgemisch die maximal Tauchtiefe berechnen


**Beispiel:** Du tauchst mit mit Nx40 / EAN40, wie tief darf dein Tauchgang maximal sein?     

     p_G     = p  · f_G                 Definition Partialdruck
     1,4 bar = p  · 0,4                 p_G = 1,4 bar (max. zulässiger p_O2), f_G = 0,4 (Sauerstoffanteil 40%)
     p       = 1,4 bar / 0,4 = 3,5 bar (max. Umgebungsdruck)


Ein maximaler Umgebungsdruck von 3,5 bar entspricht maximalen einer Tauchtiefe von 25m
