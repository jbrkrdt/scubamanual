---
title: Konstant-Beimischverfahren
---

# Konstant-Beimischverfahren[^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung

![](assets/2303171703-Beimengmethode.svg){width=500px align=right}

- Sauerstoff und Luft werden bei Umgebungsdruck gemischt und auf das entsprechende Zielgemisch gebracht.
- Der Kompressor saugt dann das fertige Gemisch an und bringt es auf den gewünschten Enddruck.
- Danach erfolgt eine weitere Gasanalyse.

Vorteile: 

- O2-Handling nur bei niedrigem Druck
- einfacher, schnellere Füllen vieler Flaschen (ggf. Kaskade)

Nachteile:

- Nitrox-tauglicher Kompressor (teuer!)
- Mischungen nur bis 40% Sauerstoff möglich.
- O2-Logistik weiter nötig



