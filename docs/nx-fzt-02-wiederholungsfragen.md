# Wiederholungsfragen

- Was ist Nitrox?
- Welche Zusammensetzung hat EAN32? Nx36?
- Wie hoch ist der maximal zulässige Sauerstoffpartialdruck für einen Sporttaucher, unter normalen Bedingungen?
- Welche Symptome können bei einem Sauerstoffkrampf auftreten?
- Was bedeutet der Begriff „sauerstofftauglich“?.
- Welche Fülltechniken für Nitrox kennst du?
- Erkläre die MOD. Berechne die MOD für einen Tauchgang mit Nx32, Nx36?
- Erkläre die EAD. Berechne die EAD für einen Tauchgang auf 20 Meter mit Nitrox 36.
- Nenne 3 Möglichkeiten zur Ermittlung Deiner Dekodaten
- Verwendung des Tauchgangsplaners
     - Bestimme mit Hilfe des Tauchgangs-Planers die MOD für einen Tauchgang mit Nitrox 36
     - Bestimme mit Hilfe des Tauchgangsplaners die maximale Tauchzeit für einen Tauchgang, unter Ausnutzung der MOD
     - Bestimme mit Hilfe des Tauchgangs-Planers die EAD für einen Tauchgang auf 22 Meter mit Nitrox 32



