---
title: Tauchgangsplanung
---

# Tauchgangsplanung [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221829-tiefe-grundzeit-festlegen.svg](assets/2403221829-tiefe-grundzeit-festlegen.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow: 3">

Geplante Tiefe und Grundzeit sind für die Tauchgangsplanung ausschlaggebend

![2403221847-tauchprofil.svg](assets/2403221847-tauchprofil.svg)

</div>
</div>