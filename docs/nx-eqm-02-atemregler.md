---
title: Nitrox-Atemregler
---

# Nitrox-Atemregler [^1] ![ATEX XTX50 Nitrox](https://de.apeksdiving.com/cdn/shop/files/RG115112_1.jpg){width=500px align=right}



Bei Nitrox-Atemreglern sind alle Bauteile sauerstofftauglich! 

- sauerstofftaugliche O-Ringe
- Mitteldruckschlauch (sauerstofftauglich
- Nitrox-Gewinde (M26x2) in EU gemäß EN 144-3 und EN 13949 für 21-100% O2[^2]



[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung
[^2]: [APEKS, XTX50 Nitrox - Atemregler](https://de.apeksdiving.com/products/xtx50-nitrox-dive-regulator)o





