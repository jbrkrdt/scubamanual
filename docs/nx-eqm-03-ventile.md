---
title: Nitrox-Ventile
---

# Nitrox-Ventile [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung

![2403120656-nitrox-flasche](assets/2403120656-nitrox-flasche.jpg){align=right}

- Nitrox-Ventilen sind alle Bauteile sauerstofftauglich! 
- Ventilgewinde M26x2 in EU gemäß EN 144-3 und EN 13949 für 21-100% O2[^2]
- Flasche sauerstoffrein (Handling und Ölfreiheit beim Füllen, ggf. Personal-Filter)

[^2]: [APEKS, XTX50 Nitrox - Atemregler](https://de.apeksdiving.com/products/xtx50-nitrox-dive-regulator)o



