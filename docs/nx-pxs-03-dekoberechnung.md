---
title: Dekoberechnung
---

# Dekoberechnung [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

- Verwendung einer **Luft-Tauchcomputers** (oder einer Luft-Dekotabelle)
    - :fontawesome-regular-face-smile: konservativ bez. Deko: Nullzeit und Dekoverpflichtung werden unterschätzt -> Sicherheitsreserver für Risikopersonen
    - :fontawesome-regular-face-meh: CNS-O2% werden unterschätzt (händische Kontrolle notwendig

- **Ausnutzung der Nullzeiten** durch Verwendung einer **Nitrox-Tauchcomputers**,  Luft-Dekotabelle unter Berücksichtigung der EAD oder einer Nitrox-Dekotabelle

    - :fontawesome-regular-face-smile: Größere Nullzeiten und geringere Dekozeiten, die mögliche Tauchzeit wird länger 
      - :fontawesome-regular-face-meh: Die Aufsättigung mit Stickstoff entspricht der eines Nullzeittauchgangs mit Druckluft (der Sicherheitsaspekt geht verloren)
    - :fontawesome-regular-face-meh: Mikrogasblasenbildung ist vorhanden, da sich die Tauchzeit verlängert



