---
title: Best Mix
---

# Best Mix [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653) 2-Physikalische Grundlagen und [Wikipedia](quellen.md#2403150938) [Nitrox](https://en.wikipedia.org/wiki/Nitrox)

- **Best Mix** bezeichnet ein Atemgas bezeichnet, welches die Dekompressionverpflichtung auf der geplanten Tauchtiefe minimiert
- Minimale Aufsättigung erreicht man mit einem *niedrigen N2-Partialdruck* bzw. einem ein *maximaler O2-Partialdruck*, 
- Für Sporttauchgänge wird eine maximaler Sauerstoffpartialdruck von 1,4 bar empfohlen.
- Mithilfe der Formel für den Partialdruck und dem maximaler empfohlnen Sauerstoffpartialdruck von 1,4 bar, lässt sich daraus für jedes Gasgemisch die maximal Tauchtiefe berechnen


**Beispiel:** Du möchtest an einem Wrack auf 28  Meter tauchen. Welche Mischung fG wäre hier am geeignetsten?


     p_G     = p       · f_G                 Definition Partialdruck
     1,4 bar = 3,8 bar · f_G                 p_G = 1,4 bar (max. zulässiger p_O2), p = 3,8 bar (Umgebungsdruck auf 28m)
     f_G     = 1,4 bar / 3,5 bar = 0,368     max. O2-Anteil

Dies entspricht einem Nitroxgemisch mit einem Sauerstoffanteil von 37 %


