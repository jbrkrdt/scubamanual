---
title: Zusammenfassung
---

# Zusammenfassung	[^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 1-Einführung

- Nitrox ist ein Gas mit erhöhtem Sauerstoffanteil (gegenüber Luft)
- Gängige Bezeichnungen sind Nx32, EAN36, Nitrox40


## Vorteile

- die  Stickstoffsättigung des Körpers ist geringer, damit ist der Tauchgang *gesünder*
- verlängert sich die Nullzeit bzw. die Dekompressionszeit sinkt (**Verhindert nicht den Tiefenrausch!**) 

## Nachteile

- zusätzliche Ausbildung nötig
- maximal zulässige Tauchtiefe muss eingehalten werden (**Nitrox ist kein Tieftauchgas**) [^2]
- aufgrund des erhöhten O2-Gehaltes ist ein besonders sorgfältiger Umgang mit Ausrüstung notwendg
- in Ländern der Europäischen Unition ist eine besondere Nitrox-Ausrüstung erforderlich

[^2]: auch Luft hat eine MOD, diese liegt aber > 40 Meter.