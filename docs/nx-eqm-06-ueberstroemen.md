---
title: Überströmen
---
# Überströmen[^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung

![2403121901 Bank of high pressure storage cylinders for nitrx and trimix decanting](https://upload.wikimedia.org/wikipedia/commons/d/d1/Bank_of_high_pressure_storage_cylinders_for_nitrx_and_trimix_decanting_IMG_20190411_172351.jpg){align=right width=500px}

- vom Gaslieferanten gefüllte Kaskaden dienen als Speicher 
- Tauchflasche wird aus dem Speicher befüllt
- keine individuellen Mischungen möglich
- einfaches und schnelles Verfahren

TODO Skizze nitrox-überströmen
