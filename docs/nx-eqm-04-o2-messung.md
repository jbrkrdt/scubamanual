---
title: Sauerstoffmessung
---

# Sauerstoffmessung [^1]

- unterschiedliche  Messgerät mit verschiedenen Messverfahren
- verbreitet Messverfahren Elektrochemische Zelle: in der elektroschemischen Zelle wird Sauerstoff in chemischer Reaktion verbraucht, dabei fließt ein Strom, der gemessen werden kann
    - vergleichbar günstig
    - Zellenalterung, regelmäßig kalibriert und Tausch nach 1-2 Jahren
    - Querempfindlichkeiten (Gase führen zur Alternung, Kälte, Trägergas)
    - Druckabhängigkeit (O2-Diffussion durch eine Membran, Feuchte)




[^1]: file:///C:/Users/Jost/Downloads/September_2012_-_O2-Messung___neue_Webseite.pdf
[^2]:https://industrialphysics.com/de/wissenbasis/artikell/elektrochemische-zellen-fuer-sauerstoff-analysatoren/
[^3]:https://dokumente.ub.tu-clausthal.de/servlets/MCRFileNodeServlet/clausthal_derivate_00000240/Buchbeitrag_1985Plu.pdf
[^4]:https://www.dks-engineering.com/de/wissen/feuchte-bestimmen-im-sauerstoff
[^5]:https://www.xylemanalytics.com/File%20Library/Resource%20Library/WTW/04%20Flyers%20Brochures/DEU/WTW_999203D_Oxi_Fibel_A5_de_low.pdf