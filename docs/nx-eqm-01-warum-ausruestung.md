---
title: Warum Nitrox-Ausrüstung?
---

# Warum Nitrox-Ausrüstung? [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung

![Stefan-Xp, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/3/34/Verbrennungsdreieck.svg){ width=400px align=right }

Sauerstoff ist ein starkes Oxidationsmittel [^2], das bedeutet:

[^2]: https://de.wikipedia.org/wiki/Oxidationsmittel


- Sauerstoff ist stark brandförderndes
    - Entflammen entsteht viel leichter (z.B. schon bei niedrigeren Temperaturen)
    - Materialen, die bei Normalbedingungen nur schwer entzündbar sind, können brennen.
- Der Kontakt mit Sauerstoff kann Materaleigenschaften verändern

Für die Tauchausrüstung heisst das:

- nur bestimmte (sauerstofftaugliche) Materialen dürfen verwendet werden ( bestimmte Metalle, O-Ringe)
- die Ausrüstung muss gereinigt und fettfrei sein (sauerstoffrein: frei von Schmutz, keine  oder nur sauerstoffverträgliche Fette)
- muss pfleglicher behandelt werden:
    - keine Fingerabdrücke (Hautfette!) 
    - keine Druckstöße beim Öffnen der Ventile (adiabate Temperaturerhöhung
    - keine Zündquellen, offenen Flammen (Bsp. Entzündung des Unterzieher)


!!! note "sauerstofftauglich  =  sauerstoffrein  und  sauerstoffverträglich"

    Nur wenn die Nitroxausrüstung aus sauerstoffverträglichen Materialien gefertigt und gereinigt (sauerstoffrein) wurde, ist sie sauerstofftauglich. <br>	.
    sauerstofftauglich  =  sauerstoffrein  und  sauerstoffverträglich

!!! note EN 144-3 und EN 13949

    Nitrox-Ausrüstung wird in Europa gemäß EN 144-3 und EN 13949 bereitgestellt, die vorgeben, dass die in Ländern der Europäischen Gemeinschaft verkaufte Ausrüstung für 21 bis 100 % O2 mit einem speziellen M26-Anschluss ausgestattet sein muss.

[^2]: [APEKS, XTX50 Nitrox - Atemregler](https://de.apeksdiving.com/products/xtx50-nitrox-dive-regulator)

