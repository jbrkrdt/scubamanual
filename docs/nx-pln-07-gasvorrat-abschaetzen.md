# 6. Abschätzung Gasvorrat

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221836-abschaetzung-gasvorrat.svg](assets/2403221836-abschaetzung-gasvorrat.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow: 3">
- **verfügbares Gasvolumen** = Flaschenvolumen * Fülldruck / 1bar **- Gasreserve**
- **mögliche Tauchzeit** =
     **verfügbares Gasvolumen** / **mittlerer Umgebungsdruck** auf geplanter Tauchtiefe / **Atemminutenvolumen**

</div>
</div>
