---
title: Partialdruckmethode
---

# Partialdruckmethode[^1]

[^1]: vgl [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 5-Ausrüstung

![](assets/2303171618-Partialdruckmethode.svg){width=500px, align=right}

1. Die Tauchflasche wird mit Sauerstoff vorgefüllt.
2. Ein Druckluftkompressor füllt die Tauchflasche mit Luft auf.


- Die Menge an vorgefülltem Sauerstoff und der Endfülldruck des Kompressors bestimmen die Nitroxmischung.
- Beliebige Mischungsverhältnisse sind möglich.



