---
title: Was ist Nitrox? 
---

# Was ist Nitrox? 

Nitrox oder Enriched Air Nitrox (EAN oder EANx) ist ein Atemgasgemisch aus Stickstoff (engl. nitrogen) und Sauerstoff (engl. oxygen) mit einem höheren Sauerstoffanteil als normale Luft (in der Regel zwischen 32 % und 40 % statt 21 %).[^1]

![](assets/2403130121-zusammensetzung-nitrox.png)

[^1]: [Wikipedia, Nitrox](https://de.wikipedia.org/wiki/Nitrox), abgerufen am 13.03.2024

