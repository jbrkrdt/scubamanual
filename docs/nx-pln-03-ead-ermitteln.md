# 2. Ermittlung EAD	

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221831-EAD-ermitteln.svg](assets/2403221831-EAD-ermitteln.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow:3">
- Wenn mit Lufttabelle geplant werden soll, aus EAD-Tabelle ablesen

       ![2403221852-ead-ablesen.svg](assets/2403221852-ead-ablesen.svg){align=center}

- oder berechnen…. 
</div>

</div>


