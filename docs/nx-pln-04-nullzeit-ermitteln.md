# 3. Ermittlung Nullzeit, Deko

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221832-NZ-ermitteln.svg](assets/2403221832-NZ-ermitteln.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow: 3">

- Nullzeit ablesen, ggf. Deko-Stops bestimmen

    ![2403221853-nullzeit-deco2000.svg](assets/2403221853-nullzeit-deco2000.svg)

- Zeitzuschlag bei Wiederholungs- Tauchgängen berücksichtigen!
- Alternative: Nitrox-Planner Tauchcomputer,
- Austauchzeit ermitteln: Aufstieg (1min/10m) + 3 min Sicherheitsstop + ggf. Dekostops
</div>
	
</div>
