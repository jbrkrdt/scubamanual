# 4. Ermittlung CNS Belastung


<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>
![2403221833-cns-belastung-ermitteln.svg](assets/2403221833-cns-belastung-ermitteln.svg)
</div>

<div markdown style="flex-basis:50%; flex-grow:3">


- pO2_max -> und Tzmax oder CNS O2%/min 

        CNS-%    = CNO2%/min * Tauchzeit
        oder     = Tzmax     / Tauchzeit

    Beispiel: 
     pO2 = 1,4,
     CNS O2 %/min = 0,65,
     Tauchzeit = 40 min


     -> 40 min x 0,65 = 26% CNS O2


- Bei WDH-TG Rest-CNS berücksichtigen! (Halbwertszeit 90 min)

      ![2403230819-noaa-cns-o2-tabelle.svg](assets/2403230819-noaa-cns-o2-tabelle.svg)
</div></div>