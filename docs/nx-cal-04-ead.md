---
title: EAD
---

# EAD   (Equivalent Air Depth) [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 2-Physikalische Grundlagen

- Bei Nitrox-Tauchgängen ist die Aufsättigung mit N2 geringer als bei Tauchgängen mit Pressluft, Lufttabellen sind daher nicht direkt einsetzbar.
- Die *EAD* (**Equivalent Air Depth** bzw. **äquivalente Lufttiefe**) ermöglicht die Verwendung von Lufttabbellen, in die tatsächliche Tauchtiefe auf die Tauchtiefe eines *äquivanten* (flacheren) Lufttauchgang umgerechnet wird.
- Mithilfe der Formel für den Partialdruck und kann die äquivalente Lufttiefe berechnet werden.

          
```
     p_N2 = P   · f_N2    (N2-Partialdruck beim Nx-Tauchgang, wobei Stickstoffanteil f_N2 = 1 - Sauerstoffanteil f_O2 
     ...  = EAD · 0,79     ... identisch zum Partialdruck beim  äquivalenten Luft-Tauchgang)
```

Durch Umstellen ergibt sich die Formel für die EAD

```
     EAD = P ·f_N2 / 0,79 
```

**Beispiel:** Du tauchst auf 20 m mit einem Nitroxgemisch von 40% Sauerstoff. Welche Tiefenstufe hättest du mit Luft?
  
```
     f_N2 = 1 - f_O2 = 1 - 0,4 = 0,6  (Stickstoffanteil im Nx40-Gemisch 60%)

     EAD  = P     · f_N2 / 0,79
          = p     · 0,60 / 0,79       (O2-Anteil = 40% -> Stickstoff 60%)
          = 3 bar · 0,60 / 0,79       (Umgebungsdruck afu 20m Tiefe = 3 bar)
          = 2,3 bar                   (exakt 2,2384 bar, die EAD zur größeren Tauchtiefe aufrunden!!)
```

Dies entspricht einem Umgebungsdruck von 2,3 bar und damit einem Lufttauchgang auf 13 m.


