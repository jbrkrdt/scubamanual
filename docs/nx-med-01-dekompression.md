---
title: Dekompression
---

# Dekompression [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 3-Medizin

Die Aufsättigung mit Stickstoff ist bei einem Nitroxtauchgang im Vergleich zu einem Drucklufttauchgang 
**mit genau gleichem Profil** aufgrund des reduzierten Partialsdrucks immer geringer!

- :fontawesome-regular-face-smile: Geringere Aufsättigung mit Stickstoff 
- :fontawesome-regular-face-smile: Geringere Mikrogasblasenbildung
