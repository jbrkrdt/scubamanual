---
title: Sauerstoffgiftigkeit
---


# Sauerstoffgiftigkeit [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 3-Medizin


- Unter normalem Druck ist Sauerstoffatmung über mehrere Stunden möglich
- Sauerstoffgiftigkeit nimmt unter Druck zu
- Sauerstoffgiftigkeit nimmt bei langer Einwirkdauer zu
- Schädigung des Zentralnervensystem (ZNS)
- Schädigung der Lunge
