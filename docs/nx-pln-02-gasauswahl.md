# 1. Gasauswahl

<div style="display:flex; flex-wrap:wrap; justify-content:flex-start; gap:20px " markdown> 

<div markdown>

![2403221830-gasauswahl.svg](assets/2403221830-gasauswahl.svg	)

</div>

<div markdown style="flex-basis:50%; flex-grow:3">

- verfügbare Gase, Best-Mix oder Standardgase
- mit pO"_max ound %O2 aus Tabelle

      ![2403221834-mod.svg](assets/2403221834-mod.svg)

- oder berechnen

</div>
</div>

