---
title: Paul-Bert-Effekt
---

# Paul-Bert-Effekt [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 3-Medizin

- Gefahr von Krampfanfällen bei pO2 > 1,6 bar
      - über Wasser nicht Lebensbedrohlich
      - unter Wasser kann zum Verlieren des Mundstücks führen
- Krampfanfall kann Vorboten haben (Merkhilfe „SOZIUS“)
      - **S**chwindel
      - **O**hrengeräusche
      - **Z**ittern/Zucken der Muskulatur
      - **I**rritation des Bewusstseins  
      - **U**ebelkeit
      - **S**ichtprobleme

- Vorboten beachten -> höher Tauchen 


