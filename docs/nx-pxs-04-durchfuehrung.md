---
title: Durchführung (während des Tauchgangs)
---

# Durchführung (während des Tauchgangs) [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

- die MOD unbedingt einhalten
- auf alles achten, was auch beim Tauchen mit Luft wichtig ist
    - **Nullzeiten** können unterschiedlich sein!   


!!! note "Alle Regeln wie bei Pressluft beachten"

	- Du musst genügend Flüssigkeit zu dir nehmen.Dabei kann Alkohol die Gefahr eines Dekounfalls fördern!
	- Tauche grundsätzlich nicht bei Unwohlsein!
	- Achte immer auf angemessenen Kälteschutz!

