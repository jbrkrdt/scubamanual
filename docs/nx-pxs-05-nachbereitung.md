---
title: Nachbereitung (nach dem Tauchgang)
---

# Nachbereitung (nach dem Tauchgang) [^1]

[^1]: [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 4-Tauchgangsplanung

- Tauchgangsdaten im Logbuch notieren, inkl.:
    - verwendetes Nitroxgemisch 
    - Rest CNS-O2% Daten

- Restdruck auf dem Label der Flasche notieren

