---
title: Quellen
---

# Quellen


## Präsentationen

- [VDST, DTSA Nitrox 1Stern, 2023](){#2403101653}

## Online

- [VIT, www.taucherpedia.info](){#2403150929}
- [Wikipedia](){#2403150938}
    - [[Diving Air Compressor](https://en.wikipedia.org/wiki/Diving_air_compressor)

## Bücher

- [VDST Nitrox Manual](https://www.vdst.de/produkt/nitrox-manual/{#2403150143}

## Bildverzeichnis

Alle Bilder sind im Untertitel mit einer eindeutigen ID versehen, nutze die Suchfunktion, um die Quellenangaben zu finden.

- 2403120656-nitrox-flasche.jpg, [latiendadelbuceo](https://www.flickr.com/photos/latiendadelbuceo/latiendadelbuceo),  [CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/), 8447635760_5c0773a730_o.jpg, geladen am 12.02.2024
- [2403121901 Bank of high pressure storage cylinders for nitrx and trimix decanting](https://commons.wikimedia.org/wiki/File:Bank_of_high_pressure_storage_cylinders_for_nitrx_and_trimix_decanting_IMG_20190411_172351.jpg), Peter Southwood, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons
- [2403130135 EANx](https://commons.wikimedia.org/wiki/File:EANxDecal.png), CC BY-SA 3.0 Deed
- 2403121411-tauchflaschen.jpeg

## Wikipedia

- 2310020001 Gas_cylinder_-_ISO_10628-2.svg
- 2310020002-Hose_(ISO_10628-2).svg https://de.wikipedia.org/wiki/Datei:Hose_(ISO_10628-2).svg
- 2310020003-Valve,_angle_globe_type_-_ISO_10628-2.svg https://de.wikipedia.org/wiki/Datei:Valve,_angle_globe_type_-_ISO_10628-2.svg
- 2310020004-Compressor,_vacuum_pump,_reciprocating_piston_type_(ISO_10628-2).svg https://de.wikipedia.org/wiki/Datei:Compressor,_vacuum_pump,_reciprocating_piston_type_(ISO_10628-2).svg
- 2310020005-Discrete_instrument_(field).svg https://commons.wikimedia.org/wiki/File:Discrete_instrument_(field).svg