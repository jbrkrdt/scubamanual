---
title: Nitrox Bezeichnungen 
---

# Nitrox Bezeichnungen  [^1]

[^1]: vgl. [VIT, Taucherpedia, Enriched_AIR_Nitrix](https://www.taucherpedia.info/wiki/Enriched_Air_Nitrox)



||Abk.|Internationale Bezeichnung  | Sauerstoff | Stickstoff
|-|-|-|-|-|
|Luft|-|Air| 21%|79%|
|Nitrox 28|Nx28|EAN28|28%|68%|
|Nitrox 32|Nx32|EAN32, NOAA 1|32%|68%|
|Nitrox 36|Nx36|EAN36, NOAA 2|36%|64%|
|Nitrox 50|Nx50|EAN50, Safe Air|50%|50%|
