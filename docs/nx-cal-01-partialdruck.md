---
title: Partialdruck einer Gaskomponente
---

# Partialdruck einer Gaskomponente [^1]

[^1]: vgl. [VDST, DTSA Nitrox 1Stern, 2023](quellen.md#2403101653), 2-Physikalische Grundlagen

![](assets/2403100304-partialdruck.svg)

    p_G = p · f_G	

    wobei
    p_G   = Druck (engl. pressure) des jeweiligen Gases (Partialdruck)
    p  	  = Umgebungsdruck
    f_G	  = Anteil (engl. fraction) des Einzelgases


**Beispiel:** Wie groß ist der Partialdruck des Sauerstoffs in deinem Atemgas, wenn du mit Nx32 (Nitroxgemisch mit 32% Sauerstoff und 68% Stickstoff) auf 30m Tiefe tauchst?

     p_G = p       · f_G      Definition Partialdruck
     p_G = 4 bar   · 0,32     p = 4 bar (Umgebungsdruck in 30m Tiefe) und f_G = 0,32 (Sauerstoffanteil 32q%)
     p_G = 1,28 bar


Du hast somit auf dieser Tiefe einen Sauerstoffpartialdruck von ca. 1,3 bar.
